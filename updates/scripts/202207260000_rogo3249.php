<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Adds an index to the sid table to improve the speed of some user search queries.
 *
 * @author Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright Copyright (c) 2022 The University of Nottingham
 */

if ($updater_utils->check_version('7.5.0')) {
    if (!$updater_utils->has_updated('rogo_3246')) {
        $altersql = 'ALTER TABLE sid
            ADD KEY `student_id` (`student_id`)';
        $updater_utils->execute_query($altersql, false);

        $updater_utils->record_update('rogo_3246');
    }
}
