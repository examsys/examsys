<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

namespace testing\unittest;

use InstallUtils;
use Config;
use cli_utils;
use mysqli;

/**
 * This class is used to manage the database used for ExamSys unit testing.
 *
 * Based on /testing/behat/classes/database.php by Neill Magill <neill.magill@nottingham.ac.uk>
 *
 * @author Dr Joseph baxter <joseph.baxter@nottingham.ac.uk>
 * @copyright Copyright (c) 2016 The University of Nottingham
 * @package testing
 * @category unittest
 */
class database
{
    /**
     * Creates a ExamSys database for phpunit testing.
     *
     * @throws \Exception
     */
    public static function install_database()
    {
        $config = Config::get_instance();
        $config->use_phpunit_site();
        InstallUtils::$cli = true;
        InstallUtils::$phpunit_install = true;
        // Check that the php environment is setup correctly.
        try {
            \requirements::check();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        // Setup the InstallUtils class for installation.
        InstallUtils::$cfg_db_basename = $config->get('cfg_db_database');
        InstallUtils::$cfg_db_name = $config->get('cfg_db_database');
        InstallUtils::$cfg_web_host = $config->get('cfg_web_host');
        InstallUtils::$cfg_rogo_data = $config->get('cfg_phpunit_data');
        $connected = self::get_db_details();
        if (!$connected) {
            throw new \Exception('Could not connect to database. Aborting.');
        }

        // Check database version is supported.
        if (!\requirements::check_db($config->get('cfg_db_host'), $config->get('cfg_phpunit_db_user'), $config->get('cfg_phpunit_db_password'), $config->get('cfg_db_port'))) {
            $mysql_min_ver = $config->getxml('database', 'mysql', 'min_version');
            throw new \Exception('MySQL version does not meet minimum requirement - ' . $mysql_min_ver);
        }

        // Preset the database usernames to the details of the live site.
        InstallUtils::$cfg_db_username = $config->get('base_database') . '_auth';
        InstallUtils::$cfg_db_student_user = $config->get('base_database') . '_stu';
        InstallUtils::$cfg_db_staff_user = $config->get('base_database') . '_staff';
        InstallUtils::$cfg_db_external_user = $config->get('base_database') . '_ext';
        InstallUtils::$cfg_db_sysadmin_user = $config->get('base_database') . '_sys';
        InstallUtils::$cfg_db_webservice_user = $config->get('base_database') . '_web';
        InstallUtils::$cfg_db_sct_user = $config->get('base_database') . '_sct';
        InstallUtils::$cfg_db_inv_user = $config->get('base_database') . '_inv';
        InstallUtils::$cfg_db_internal_user = $config->get('base_database') . '_int';
        InstallUtils::$cfg_cron_user = 'cron';

        // Details of the admin user.
        InstallUtils::$sysadmin_username = 'admin';
        InstallUtils::$sysadmin_password = 'admin';
        InstallUtils::$sysadmin_first = 'Admin';
        InstallUtils::$sysadmin_last = 'User';
        InstallUtils::$sysadmin_title = 'Miss';
        InstallUtils::$sysadmin_email = 'admin@example.com';

        // Ensure that an existing ExamSys phpunit database and users are deleted.
        self::drop_db();

        // Start installing the base ExamSys database.
        InstallUtils::checkDBUsers();
        InstallUtils::createDirectories();
        InstallUtils::createDatabase(
            $config->get('cfg_db_database'),
            $config->get('cfg_db_charset'),
            $config->get('cfg_db_collation'),
            $config->get('cfg_phpunit_db_engine'),
            $config->get('cfg_phpunit_db_help_engine')
        );
        // Create constraints.
        InstallUtils::createConstraints();
    }

    /**
     * Gets the database admin username and password.
     *
     * @return boolean
     */
    public static function get_db_details()
    {
        $config = Config::get_instance();
        cli_utils::prompt('Database setup');
        InstallUtils::$db_admin_username = $config->get('cfg_phpunit_db_user');
        InstallUtils::$db_admin_passwd = $config->get('cfg_phpunit_db_password');
        $connected = self::connect_database(InstallUtils::$db_admin_username, InstallUtils::$db_admin_passwd);
        return $connected;
    }

    /**
     * Drop the phpunit database and users.
     */
    public static function drop_db()
    {
        $config = Config::get_instance();
        $config->use_phpunit_site();
        $basedb = $config->get('base_database');
        // If it exists drop the phpunit database.
        $dbname = InstallUtils::$cfg_db_name;
        $dbaccesspoint = InstallUtils::$cfg_web_host;
        $res = InstallUtils::$db->prepare("SHOW DATABASES LIKE '$dbname'");
        $res->execute();
        $res->store_result();
        if ($res->num_rows > 0) {
            InstallUtils::$db->query("DROP DATABASE $dbname");
        }

        // Remove permissions from the DB users.
        $usernames = ['auth' => 300, 'stu' => 301, 'staff' => 302, 'ext' => 303, 'sys' => 304, 'sct' => 305, 'inv' => 306];
        foreach ($usernames as $username => $err_code) {
            $test_username = $basedb . '_' . $username;
            if (InstallUtils::does_user_exist($test_username)) {
                try {
                    InstallUtils::$db->query("REVOKE ALL PRIVILEGES ON $dbname.* FROM '$test_username'@'$dbaccesspoint'");
                } catch (\mysqli_sql_exception $e) {
                    // The privilege was already revoked.
                }
            }
        }
    }

    /**
     * Connect to the ExamSys database.
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public static function connect_database($username, $password)
    {
        $config = Config::get_instance();
        $config->use_phpunit_site();
        InstallUtils::$db = new mysqli($config->get('cfg_db_host'), $username, $password, '', $config->get('cfg_db_port'));
        if (mysqli_connect_error()) {
            InstallUtils::$db = null;
            return false;
        }
        return true;
    }
}
