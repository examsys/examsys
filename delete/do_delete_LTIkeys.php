<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Delete an LTI key - SysAdmin only.
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require '../include/sysadmin_auth.inc';
require_once '../include/errors.php';
require_once '../LTI/ims-lti/UoN_LTI.php';

$LTIkeysid = check_var('LTIkeysID', 'POST', true, false, true);

$lti = new UoN_LTI();
$lti->init_lti0($mysqli);

if (!$lti->lti_key_exists($LTIkeysid)) {
    $contactemail = support::get_email();
    $msg = sprintf($string['furtherassistance'], $contactemail, $contactemail);
    $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
}

$lti->delete_lti_key($LTIkeysid);

$render = new render($configObject);
$lang['title'] = $string['title'];
$lang['success'] = $string['ltikeydel'];
$data = [];
$render->render($data, $lang, 'admin/do_delete.html');
