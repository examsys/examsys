<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require_once '../include/load_config.php';
$deb32434 = 1;

$configObject = Config::get_instance();
$cfg_web_root = $configObject->get('cfg_web_root');
?>
    <item title="<?php echo(StripForTitle($title)) ?>" ident="<?php echo $question->load_id ?>">
        <itemmetadata>
            <qmd_itemtype><![CDATA[<?php echo $type ?>]]></qmd_itemtype>
            <qmd_status><![CDATA[<?php echo $question->status ?>]]></qmd_status>
            <qmd_score_method><![CDATA[<?php echo $question->score_method ?>]]></qmd_score_method>
            <qmd_toolvendor><![CDATA[ExamSys <?php echo $configObject->get_setting('core', 'rogo_version'); ?>]]></qmd_toolvendor>
        </itemmetadata>

        <presentation>

<?php if ($question->author) : ?>
            <qticomment><![CDATA[Author:<?php echo $question->author ?>]]></qticomment>
<?php endif; ?>
<?php if ($question->q_group) : ?>
            <qticomment><![CDATA[Module:<?php echo $question->q_group ?>]]></qticomment>
<?php endif; ?>
<?php if ($question->bloom) : ?>
            <qticomment><![CDATA[Blooms:<?php echo $question->bloom ?>]]></qticomment>
<?php endif; ?>
<?php if (count($question->keywords) > 0) : ?>
    <?php foreach ($question->keywords as $keyword) : ?>
        <?php if (trim($keyword) != '') : ?>
            <qticomment><![CDATA[Keyword:<?php echo $keyword ?>]]></qticomment>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
