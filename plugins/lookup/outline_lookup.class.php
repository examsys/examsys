<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * The outline lookup class
 *
 * @author Simon Atack
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

$configObject = Config::get_instance();

class outline_lookup
{
    protected $name;
    protected $number;
    protected $form;
    protected $settings;
    protected $db;
    protected $calling_object;
    public $debug = [];
    public $debugpointer = 0;
    protected $error = null;
    public $rogoid = false;
    protected $lookupapiversion;
    protected $callbackarray;
    protected $impliments_api_lookup_version = 0;

    /**
     * Called when the object is unserialised.
     */
    public function __wakeup()
    {
        // The serialised database object will be invalid,
        // this object should only be serialised during an error report,
        // so adding the current database connect seems like a waste of time.
        $this->db = null;
    }

    /**
     * @param $calling_object object its called from
     * @param $settings array settings options
     * @param $number int the number this is
     * @param $name string the name of it
     * @param $db object a lin kto db
     * @param $returndata object where data is stored
     * @param $form object a class with form data in
     */
    public function __construct($number, $name, $lookupapiversion)
    {
        $this->lookupapiversion = $lookupapiversion;
        $this->name = $name;
        $this->number = $number;
    }

    public function apicheck()
    {

        if ($this->lookupapiversion != $this->impliments_api_lookup_version) {
            $this->savetodebug('This lookup object is implementing an different version of the api than this plugin does');
            $this->set_error('Wrong API');
            return true;
        }

        return false;
    }

    public function set_error($msg)
    {
        if (mb_strlen($this->error) > 0) {
            $this->error .= '<br>';
        }
        $this->error .= $msg;
    }

    public function init($object)
    {
        $this->db = & $object->db;
        $this->calling_object = & $object->calling_object;
        //    $this->returndata = & $object->returndata;
        //    $this->retdata = & $this->returndata[$this->number];
        $this->form = & $object->form;
        $this->settings = & $object->settings;
        $this->session = & $object->calling_object->session;
        $this->request = & $object->calling_object->request;
    }

    public function error_handling($context = null)
    {

        $context1 = [];
        if (is_null($context)) {
            // if no array set get currently define variables in this object
            $context = get_defined_vars();
        }

        $context1 = error_handling($context);
        if (isset($context1['settings'])) {
            $context1['settings'] = 'hidden for security';
        }
        return $context1;
    }

    //fake function used in mocking but if things go wrong have an outline here
    public function mock($callingobject, $settings, $number, $name, $db, $returndata, $form)
    {
        return false;
    }

    /**
     * @param $debugmessage string the debug message to store
     */
    public function savetodebug($debugmessage)
    {
        $this->debug[] = $debugmessage;
    }

    /**
     * @param $section string the section to get the callback from
     *
     * @return mixed
     */
    public function get_callback($section)
    {
        return $this->calling_object->get_callback($section);
    }

    public function get_new_debug_messages($number = null)
    {
        if (is_null($number)) {
            $returnarray = [];
            while (isset($this->debug[$this->debugpointer])) {
                    $returnarray[$this->debugpointer] = $this->debug[$this->debugpointer++];
            }

            return $returnarray;
        } else {
            return $this->calling_object->lookupPluginObj[$number]->get_new_debug_messages();
        }
    }

    /**
     * @param $objid int the objectid
     *
     * @return mixed
     */
    public function get_module_lookupinfo($objid)
    {
        return $this->calling_object->lookupinfo[$objid];
    }


    public function register_callback_sections()
    {
        //this is blank so that classes that dont register anything dont break
        return [];
    }

    /**
     * @param $callback callback routine
     * @param $section string section to register callback in
     * @param $number string the number this object is
     * @param $name string the name this object is
     * @param $insert bool to insert rather than append
     *
     * @return void
     */
    public function register_callback($callback, $section, $number, $name, $insert = false)
    {
        //return $this->calling_object->register_callback($callback, $section, $number, $name, $insert);
        $this->callbackarray[] = [$callback, $section, $number, $name, $insert];
    }


    /**
     *
     */
    public function register_callback_routines()
    {
        //this is blank so that classes that dont register anything dont break
        return [];
    }

    /**
     * @param $setting string the setting to return or false if it doesnt exist
     *
     * @return mixed
     */
    public function get_settings($setting)
    {
        if (!isset($this->settings[$setting])) {
            return false;
        }

        return $this->settings[$setting];
    }

    public function get_info()
    {
        $data = new stdClass();
        $data->name = $this->name;
        $data->number = $this->number;
        $data->classname = get_class($this);
        $data->classname = mb_substr($data->classname, 0, mb_strpos($data->classname, '_auth'));
        $data->version = $this->version;
        $data->settings = $this->settings;
        $data->api_implimented = $this->impliments_api_lookup_version;
        $data->error = $this->error;
        if (isset($this->callbackarray)) {
            foreach ($this->callbackarray as $callback) {
                $funcname = $callback[0][1];
                $where = $callback[1];
                $data->callbackfunctions[] = [$funcname, $where];
            }
        }

        return $data;
    }
}
