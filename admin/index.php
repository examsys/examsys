<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require '../include/sysadmin_auth.inc';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
<title><?php echo page::title('ExamSys: ' . $string['admin']); ?></title>

<link rel="stylesheet" type="text/css" href="../css/header.css" />
<link rel="stylesheet" type="text/css" href="../css/body.css" />
<link rel="stylesheet" type="text/css" href="../css/admin.css" />

<script id="rogoconfig" data-lang="<?php echo \LangUtils::getLang($cfg_web_root); ?>" data-root="<?php echo $configObject->get('cfg_root_path'); ?>"></script>
<script src='../js/require.js'></script>
<script src='../js/main.min.js'></script>
</head>

<body>
<?php
  require '../include/toprightmenu.inc';

    echo draw_toprightmenu();

  // How many guest accounts are reserved
  $results = $mysqli->query('SELECT id FROM temp_users');
  $temp_account_no = $results->num_rows;
  $results->close();

  // How many system errors are there
  $results = $mysqli->query('SELECT id FROM sys_errors WHERE fixed IS NULL');
  $sys_error_no = $results->num_rows;
  $results->close();

  // How many system errors are there
  $results = $mysqli->query('SELECT id FROM save_fail_log');
  $save_fail_log_no = $results->num_rows;
  $results->close();

  // How many announcements are there
  $results = $mysqli->query('SELECT id FROM announcements WHERE startdate <= NOW() AND enddate >= NOW() AND deleted IS NULL');
  $announcement_no = $results->num_rows;
  $results->close();

  // How many papers need scheduling
  $results = $mysqli->query('SELECT property_id FROM (properties, scheduling) WHERE (start_date IS NULL AND end_date IS NULL) AND properties.property_id = scheduling.paperID AND deleted IS NULL');
  $scheduling_no = $results->num_rows;
  $results->close();

  $mysqli->close();
?>
<div id="content">

<div class="head_title">
  <div><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon" /></div>
  <div class="breadcrumb"><a href="../index.php"><?php echo $string['home'] ?></a></div>
  <div class="page_title"><?php echo $string['administrativetools'] ?></div>
</div>

<?php
if ($temp_account_no > 0) {
    $string['clearguestaccounts'] .= ' <span class="corners"><span class="num">' . $temp_account_no . '</span></span>';
}

if ($sys_error_no > 0) {
    $string['systemerrors'] .= ' <span class="corners"><span class="num">' . $sys_error_no . '</span></span>';
}

if ($save_fail_log_no > 0) {
    $string['savefailattempts'] .= ' <span class="corners"><span class="num">' . $save_fail_log_no . '</span></span>';
}

if ($announcement_no > 0) {
    $string['announcments'] .= ' <span class="corners"><span class="num">' . $announcement_no . '</span></span>';
}

if ($scheduling_no > 0) {
    $string['summativescheduling'] .= ' <span class="corners"><span class="num">' . $scheduling_no . '</span></span>';
}

  $summative_year =  date('Y');
if (date('n') < 7) {
    $summative_year--;
}

  $menudata = [];
  $menudata['audit']                = ['./audit/list_audit.php', 'audit.png'];
  $menudata['anomaly']              = ['./anomaly/settings.php', 'anomaly.png'];
  $menudata['academicsessions']     = ['academic_sessions.php', 'sessions.png'];
  $menudata['authentication']       = ['./oauth/list_oauth.php', 'auth.png'];
  $menudata['bug']                  = ['https://examsys.atlassian.net', 'bug.png'];
  $menudata['calendar']             = ['calendar.php#week' . date('W'), 'calendar_icon.png'];
  $menudata['clearguestaccounts']   = ['clear_guest_users.php', 'clear_guest_users.png'];
  $menudata['clearorphanmedia']     = ['orphan_media.php', 'remove_orphan_icon.png'];
  $menudata['cleartraining']        = ['clear_training_module.php', 'training.png'];
  $menudata['computerlabs']         = ['list_labs.php', 'computer_lab_48.png'];
  $menudata['courses']              = ['list_courses.php', 'courses_icon.png'];
  $menudata['deniedlogwarnings']    = ['view_access_denied.php', 'access_denied.png'];
  $menudata['ebelgridtemplates']    = ['list_ebel_grids.php', 'grid_48.png'];
  $menudata['faculties']            = ['list_faculties.php', 'faculty.png'];
  $menudata['imslti']               = ['../LTI/lti_keys_list.php', 'lti_key_48.png'];
if ($configObject->get_setting('core', 'cfg_ims_enabled')) {
    $menudata['imssettings']          = ['../plugins/ims/ims_settings.php', 'ims_logo.png'];
}
  $menudata['modules']              = ['list_modules.php', 'modules_icon.png'];
  $menudata['announcments']         = ['list_announcements.php', 'news_48.png'];
  $menudata['phpinfo']              = ['phpinfo.php', 'php.png'];
  $menudata['questionstatuses']     = ['list_statuses.php', 'status_icon.png'];
  $menudata['savefailattempts']     = ['list_save_fails.php', 'save_fail_48.png'];
  $menudata['schools']              = ['list_schools.php', 'school_icon.png'];
  $menudata['statistics']           = ['../statistics/index.php', 'statistics.png'];
if ($configObject->get_setting('core', 'cfg_summative_mgmt')) {  // Enable summative management scheduling if not activated.
      $menudata['summativescheduling'] = ['summative_scheduling.php', 'summative_scheduling.png'];
}
  $menudata['systemerrors']         = ['sys_error_list.php', 'system_errors.png'];
  $menudata['systeminformation']    = ['system_info.php', 'information.png'];
  $menudata['testing']              = ['../testing/', 'crash_test.png'];
  $menudata['usermanagement']       = ['../users/search.php', 'user_accounts_icon.png'];
  $menudata['plugins']       = ['./plugins/list_plugins.php', 'plugins.svg'];
  $menudata['config']        = ['config.php', 'config.png'];
  $menudata['externalsystems']        = ['external/list_extsys.php', 'sync.png'];
if ($configObject->get('cfg_setting_icons_order')) {
    foreach ($configObject->get('cfg_setting_icons_order') as $iconkey) {
        if (($iconkey == 'summativescheduling' && !$configObject->get_setting('core', 'cfg_summative_mgmt')) || empty($menudata[$iconkey])) {
            continue;
        }
        $parts = explode('.php', $menudata[$iconkey][0]);
        echo '<a class="blacklink" href="' . $menudata[$iconkey][0] . '" id="' . $parts[0] . '">';
        echo '<div class="container"><img src="../artwork/' . $menudata[$iconkey][1] . '" alt="" class="icon" /><br />' . $string[$iconkey] . '</div></a>';
    }
} else {
    foreach ($menudata as $menukey => $menuitem) {
        $parts = explode('.php', $menuitem[0]);
        echo '<a class="blacklink" href="' . $menuitem[0] . '" id="' . $parts[0] . '">';
        echo '<div class="container"><img src="../artwork/' . $menuitem[1] . '" alt="" class="icon" /><br />' . $string[$menukey] . '</div></a>';
    }
}

?>
</div>

</div>

<?php
$render = new render($configObject);
$jsdataset['name'] = 'jsutils';
$jsdataset['attributes']['xls'] = json_encode($string);
$render->render($jsdataset, [], 'dataset.html');
?>
<script src="../js/adminindexinit.min.js"></script>
</body>
</html>
