<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/module/index.php';

$string['staffmsg'] = 'This is the ExamSys student landing page. Were you looking for the <a href="../staff/">staff management screens</a>?';
$string['nopapers'] = 'You have no papers available at this time.';
$string['to'] = 'to';
$string['objectivesbased'] = 'Objectives-based feedback<br />on assessment at';
$string['questionsbased'] = 'Questions-based feedback<br />on assessment at';
$string['screen'] = 'screen';
$string['screens'] = 'screens';
$string['eassessmentmanagementsystem'] = 'eAssessment Management System';
$string['passwordRequired'] = 'password required';
$string['performsummary'] = 'Performance Summary';
