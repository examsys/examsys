<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/common.php';

$string['guestaccount'] = 'Guest Account';
$string['allocatedaccount'] = 'Allocated Account';
$string['username'] = 'Username';
$string['password'] = 'Password';
$string['login'] = 'Log In';
$string['guestaccountreg'] = 'Guest Account Registration';
$string['title'] = 'Title';
$string['firstname'] = 'First Name';
$string['surname'] = 'Surname';
$string['studentid'] = 'Student ID';
$string['denied_msg'] = 'This page can only be accessed from a university computer within the examination room.';
$string['cannotfindexams'] = 'ExamSys cannot find any exams to be taken in this lab.';
$string['msg'] = 'Please write down the log in details below in case you have to restart your computer half way through the exam.';
$string['enterfirstname'] = 'Please enter your first name.';
$string['entersurname'] = 'Please enter your surname.';
$string['error'] = 'Error';
$string['mandatory'] = 'Mandatory data missing.';
$string['nofreeaccounts'] = 'No free guest accounts';
$string['nofreeaccountsmessage'] = 'A new guest account cannot be assigned because they are all in use right now.';
$string['reservationexpired'] = 'Your reservation has expired';
$string['reservationexpiredmessage'] = 'You must create your guest account within 30 minutes, please try again.';
