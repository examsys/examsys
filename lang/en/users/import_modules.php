<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.php';

$string['impmodtitle'] = 'ExamSys: Import Modules';
$string['csvfile'] = 'CSV File:';
$string['import'] = 'Import';
$string['msg1'] = 'ExamSys can bulk upload student module enrolments. The first row should be a header row containing the following fields:';
$string['msg2'] = "The extra (optional) field 'Attempt' can be used to store attempt number for resit purposes. If left blank 1 is assumed for 1st attempt.";
$string['addingmodules'] = 'Adding Modules From';
$string['missingusers'] = 'Missing users';
$string['missingmodules'] = 'Missing modules';
$string['enrolementsperformed'] = 'Enrolments performed';
