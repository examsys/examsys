<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['osceform'] = 'OSCE Form';
$string['overallclassification'] = 'Overall Classification:';
$string['feedback'] = 'Feedback';
$string['marking3'] = ['Clear Fail', 'Borderline', 'Clear Pass'];
$string['marking4'] = ['Fail', 'Borderline Fail', 'Borderline pass', 'Pass', 'Good Pass'];
$string['marking5'] = ['Unsatisfactory', 'Competent'];
$string['marking6'] = ['Clear FAIL', 'BORDERLINE', 'Clear PASS', 'Honours PASS'];
$string['marking7'] = ['Fail', 'Pass'];
