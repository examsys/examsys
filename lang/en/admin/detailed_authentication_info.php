<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author Simon Atack
 * @version 1.0
 * @copyright Copyright (c) 2013 The University of Nottingham
 * @package ExamSys
 */

$string['detailed_authentication_information'] = 'Detailed Authentication Information';
$string['System Information'] = 'System Information';
$string['No'] = 'No';
$string['Name'] = 'Name';
$string['Class'] = 'Class';
$string['Version'] = 'Version';
$string['Settings'] = 'Settings';
$string['Function'] = 'Function';
$string['Description'] = 'Description';
$string['ID'] = 'ID';
