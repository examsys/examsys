<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['reviewsummary'] = 'Review Summary';
$string['Review Form'] = 'Review Form';
$string['Student Profile'] = 'Student Profile';
$string['title'] = 'Title';
$string['surname'] = 'Surame';
$string['firstnames'] = 'First Name(s)';
$string['studentid'] = 'Student ID';
$string['reviewed'] = 'Reviewed';
$string['reviews'] = 'Reviews';
$string['overall'] = 'Overall';
$string['Complete'] = 'Complete';
$string['Missing'] = 'Missing';
$string['q'] = 'Q';
