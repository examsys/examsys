<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['view'] = 'View hotspots on all layers';

$string['newLayer'] = 'Add Question Part';
$string['deleteLayer'] = 'Delete Question Part';
$string['edit'] = 'Move/Edit Hotspot';
$string['ellipse'] = 'Oval';
$string['rectangle'] = 'Rectangle';
$string['polygon'] = 'Polygon';
$string['erase'] = 'Erase Hotspot';

$string['colour'] = 'Colour';
$string['themecolours'] = 'Theme Colours';
$string['standardcolours'] = 'Standard Colours';

$string['yes'] = 'Yes';
$string['no'] = 'No';
$string['msgClose'] = 'Close this message';
$string['warning'] = 'WARNING';
$string['errormessage1'] = 'The polygon lines have overlapped.';
$string['errormessage2'] = 'This may result in holes in the hotspot and incorrect marking of student answers.';
$string['cancel'] = 'Do you want to cancel the last line?';
$string['errorcanvas'] = 'Canvas not supported';
$string['errorimageshotspot'] = 'Hotspot question cannot be displayed because some images were not loaded.';
