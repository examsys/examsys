<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/type.php';
require '../lang/' . $language . '/paper/new_paper2.php';

$string['uploadmarks'] = 'Upload Marks';
$string['marksloaded'] = 'Marks loaded.';
$string['msg1'] = 'CSV file should contain the columns in the following order: student_id, Question 1, Question 2...';
$string['msg2'] = 'Please select the CSV file you wish to load:';
$string['headerrow'] = 'File contains header row';
$string['errorsaving'] = 'Error updating logs';
$string['notfound'] = 'username not found!';
