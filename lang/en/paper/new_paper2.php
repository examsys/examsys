<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_types.php';
require_once '../lang/' . $language . '/paper/new_paper1.php';
require_once '../lang/' . $language . '/include/months.php';
require_once '../classes/lang/' . $language . '/papersettings.lang.php';

$string['availability'] = 'Availability';
$string['summativeexamdetails'] = 'Summative Exam Details';
$string['academicsession'] = 'Academic Session';
$string['timezone'] = 'Time Zone';
$string['from'] = 'From';
$string['to'] = 'To';
$string['modules'] = 'Module(s)';
$string['finish'] = 'Finish';
$string['msg4'] = 'There are no modules selected. Papers must be assigned to at least one module.';
$string['barriersneeded'] = 'Barriers Needed';
$string['duration'] = 'Duration';
$string['daterequired'] = 'Date required';
$string['cohortsize'] = 'Cohort Size';
$string['wholecohort'] = 'whole cohort';
$string['sittings'] = 'Sittings';
$string['campus'] = 'Campus';
$string['notes'] = 'Notes';
$string['hrs'] = 'hrs';
$string['mins'] = 'mins';

$string['msg7'] = 'WARNING: You must specify which date you require the exam to run in.';
$string['msg8'] = 'WARNING: You must specify a duration in minutes that the exam will last.';
$string['msg9'] = 'WARNING: You must specify a size for the cohort.';
$string['msg10'] = 'WARNING: A paper cannot finish before it starts';
$string['na'] = 'N/A';
