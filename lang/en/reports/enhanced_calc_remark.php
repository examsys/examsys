<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['remark'] = 'Remark';
$string['msg'] = 'To override a user\'s mark select whether to assign full, partial or no marks for the question and enter a reason for the change.';
$string['useranswers'] = 'User Answers';
$string['variables'] = 'Variables';
$string['answers'] = 'Answers';
$string['useranswer'] = 'User';
$string['units'] = 'Units';
$string['correctans'] = 'Correct';
$string['distance'] = 'Distance';
$string['marks'] = 'Marks';
$string['fullmarks'] = 'Full';
$string['partialmarks'] = 'Partial';
$string['incorrect'] = 'Incorrect';
$string['reason'] = 'Reason';
$string['na'] = 'N/A';
$string['addreason'] = 'Add reason';
$string['missingmarkmsg'] = 'Some marks had no override';
$string['nomarkmsg'] = 'No mark override selected';
$string['connectionerror'] = 'No response from the save script, changes may not have saved.';
$string['saveerror'] = 'Error saving mark. Please try again';
$string['done'] = 'Done';
