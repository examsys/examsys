<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require '../include/sysadmin_auth.inc';
require '../include/sidebar_menu.inc';
require '../include/errors.php';

$current_year = check_var('calyear', 'GET', true, false, true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
    <title><?php echo page::title('ExamSys: ' . $string['papersbyschool']); ?></title>

  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/statistics.css" />
  <link rel="stylesheet" type="text/css" href="../css/tabs.css" />

  <script id="rogoconfig" data-lang="<?php echo \LangUtils::getLang($cfg_web_root); ?>" data-root="<?php echo $configObject->get('cfg_root_path'); ?>"></script>
  <script src='../js/require.js'></script>
  <script src='../js/main.min.js'></script>
</head>

<body>
<?php
  require '../include/toprightmenu.inc';
  echo draw_toprightmenu();
  $yearutils = new yearutils($mysqli);
?>
<div id="content">
<div class="head_title">
  <div><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon" /></div>
  <div class="breadcrumb"><a href="../index.php"><?php echo $string['home']; ?></a><img src="../artwork/breadcrumb_arrow.png" class="breadcrumb_arrow" alt="-" /><a href="../admin/index.php"><?php echo $string['administrativetools']; ?></a><img src="../artwork/breadcrumb_arrow.png" class="breadcrumb_arrow" alt="-" /><a href="../statistics/index.php"><?php echo $string['statistics']; ?></a></div>
  <div class="page_title"><?php echo $string['papersbyschool']; ?>: <span style="font-weight:normal"><?php echo $_GET['calyear']; ?>/<?php echo (mb_substr($_GET['calyear'], 2, 2) + 1); ?></span></div>
</div>

<table class="header" style="font-size:90%">
<tr>
<th style="text-align:right" colspan="2">
    <div style="text-align:right; vertical-align:bottom">
        <?php
        $data = $yearutils->generateTabs($current_year, 'academic');
        $render = new render($configObject);
        $render->render($data, [], 'admin/yeartabs.html');
        ?>
    </div>
</th>
</tr>
<tr><td colspan="2" style="border:0px; background-color:#1E3C7B; height:5px"></td></tr>
</table>

<?php
$master_array = [];

$result = $mysqli->prepare("SELECT schools.id, schools.code, school, faculty.code, name FROM schools, faculty WHERE schools.facultyID = faculty.id AND school != 'Training' AND schools.deleted IS NULL AND faculty.deleted IS NULL ORDER BY name, school");
$result->execute();
$result->bind_result($id, $code, $school, $faculty_code, $faculty);
while ($result->fetch()) {
    $master_array[$code . ' ' . $school]['id'] = $id;
    $master_array[$code . ' ' . $school]['faculty'] = $faculty_code . ' ' . $faculty;
    $master_array[$code . ' ' . $school]['paper_types'] = [0, 0, 0, 0, 0, 0, 0];
}
$result->close();

foreach ($master_array as $school => $data) {
    // Get the modules which belong in the school first.
    $moduleIDs = [];

    $result = $mysqli->prepare('SELECT id FROM modules WHERE schoolid = ? AND active = 1 AND mod_deleted IS NULL');
    $result->bind_param('i', $data['id']);
    $result->execute();
    $result->bind_result($id);
    while ($result->fetch()) {
        $moduleIDs[] = $id;
    }
    $result->close();

    $master_array[$school]['module_no'] = count($moduleIDs);

    if (count($moduleIDs) > 0) {
        // Get the papers.
        $date_range = '';
        $year = param::optional('calyear', null, param::INT, param::FETCH_GET);
        if (!is_null($year)) {
            $startdatetime = date_utils::getUTCDateTime($year . '-09-01 00:00:00', 'UTC');
            $enddatetime = date_utils::getUTCDateTime($year + 1 . '-08-31 23:59:59', 'UTC');
            // Start and end within year.
            $date_range .= ' AND ((start_date > '
                . $startdatetime->getTimestamp()
                . ' AND end_date <= '
                . $enddatetime->getTimestamp() . ')';
            // Paper continuing this year.
            $date_range .= ' OR (start_date <= '
                . $startdatetime->getTimestamp()
                . ' AND end_date >= '
                . $enddatetime->getTimestamp() . ')';
            // End date within year.
            $date_range .= ' OR (start_date <= '
                . $startdatetime->getTimestamp()
                . ' AND end_date >= '
                . $startdatetime->getTimestamp()
                . ' AND end_date <= '
                . $enddatetime->getTimestamp() . ')';
            // Start date within year.
            $date_range .= ' OR (start_date >'
                . $startdatetime->getTimestamp()
                . ' AND start_date <= '
                . $enddatetime->getTimestamp()
                . ' AND end_date >= '
                . $enddatetime->getTimestamp() . '))';
        }

        $result = $mysqli->prepare('
            SELECT DISTINCT
                properties.property_id,
                paper_title,
                paper_type
                FROM
                    properties,
                    properties_modules
                WHERE
                    properties.property_id = properties_modules.property_id
                    ' . $date_range . '
                AND
                    idMod IN (' . implode(',', $moduleIDs) . ')
                AND
                    deleted IS NULL
                GROUP BY property_id
            ');
        $result->execute();
        $result->bind_result($paperID, $paper_title, $paper_type);
        while ($result->fetch()) {
            $master_array[$school]['paper_types'][intval($paper_type)]++;
        }
        $result->close();
    }
}

$old_faculty = '';
$faculty_stats = [0, 0, 0, 0, 0, 0, 0];
$newtable = false;
foreach ($master_array as $school => $data) {
    if ($old_faculty != $data['faculty']) {
        if ($old_faculty != '') {
            echo output_faculty_stats($faculty_stats);
        }
        echo '<table id="' . ltrim($data['faculty']) . '" class="stats">';
        echo '<caption class="faculty">' . $data['faculty'] . '</caption>';
        $faculty_stats = [0, 0, 0, 0, 0, 0, 0];
        $newtable = true;
    }
    echo '<tr>
    <th scope="col">' . $string['school'] . '</th>
    <th scope="col" class="papertype">' . $string['formative self-assessment'] . '</th>
    <th scope="col" class="papertype">' . $string['progress test'] . '</th>
    <th scope="col" class="papertype">' . $string['summative exam'] . '</th>
    <th scope="col" class="papertype">' . $string['survey'] . '</th>
    <th scope="col"class="papertype">' . $string['osce stations'] . '</th>
    <th scope="col"class="papertype">' . $string['offline papers'] . '</th>
    <th scope="col" class="papertype">' . $string['peer review'] . '</th>
    </tr>';
    echo '<tr id="' . ltrim($school) . '"><th scope="row">' . $school . '</th>';

    for ($i = 0; $i <= 6; $i++) {
        if ($data['paper_types'][$i] == 0) {
            echo '<td class="n grey">' . $data['paper_types'][$i] . '</td>';
        } else {
            echo '<td class="n">' . $data['paper_types'][$i] . '</td>';
        }
        $faculty_stats[$i] += $data['paper_types'][$i];
    }
    echo "</tr>\n";
    if ($newtable) {
        echo '</table>';
    }
    $old_faculty = $data['faculty'];
}
?>


</div>
<script src='../js/statisticsinit.min.js'></script>
</body>
</html>
<?php
function output_faculty_stats($stats)
{
    $html = '<tr><td>&nbsp;</td>';

    for ($i = 0; $i <= 6; $i++) {
        $html .= '<td class="n subtotal">' . number_format($stats[$i]) . '</td>';
    }

    $html .= '</tr>';

    return $html;
}
?>
