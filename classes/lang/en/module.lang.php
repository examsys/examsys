<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['showerror'] = 'System error';
$string['moduleid'] = 'Module ID';
$string['name'] = 'Name';
$string['school'] = 'School';
$string['active'] = 'Active';
$string['objapi'] = 'Objectives API';
$string['summativechecklist'] = 'Summative Checklist';
$string['smsapi'] = 'SMS API';
$string['allowselfenrol'] = 'allow Self-enrol';
$string['negativemarking'] = 'Negative Marking';
$string['ebelgrid'] = 'Ebel Grid';
$string['timedexams'] = 'Timed Exams';
$string['questionbasedfeedback'] = 'Question-based Feedback';
$string['addteammembers'] = 'Add team members';
$string['maplevel'] = 'Mapping level';
$string['academicyearstart'] = 'Academic Year Start';
$string['externalid'] = 'External ID';
$string['syncpreviousyear'] = 'Sync enrolments for previous academic year.';
